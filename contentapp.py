#!/usr/bin/python3

"""
 contentApp class
 Simple web application for managing content

 Copyright Jesus M. Gonzalez-Barahona, Gregorio Robles 2009-2015
 jgb, grex @ gsyc.es
 TSAI, SAT and SARO subjects (Universidad Rey Juan Carlos)
 October 2009 - March 2015
"""

import webapp


class contentApp (webapp.webApp):
    """Simple web application for managing content.

    Content is stored in a dictionary, which is intialized
    with the web content."""

    # Declare and initialize content
    content = {'/': 'Root page',
               '/page': 'A page'
               }

    def parse(self, request):
        """Return the resource name (including /)"""
        (comando, recurso, resto) = request.split(' ', 2)
        return (comando, recurso)

    def process(self, parsedRequest):
        """Process the relevant elements of the request.

        Finds the HTML text corresponding to the resource name,
        ignoring requests for resources not in the dictionary.
        """
        (comando, recurso) = parsedRequest

        if (comando == "GET"):
            if recurso in self.content.keys():
                httpCode = "200 OK"
                htmlBody = "<html><body>" + self.content[recurso] \
                    + "</body></html>"

            else:
                httpCode = "200 OK"
                htmlBody = \
                "<html><body>" \
                "<form name='formulario' method='post'> " \
                "<label for ='nombre' class ='form-label'> Nombre:</label>" \
                "<input type = 'text' name = 'nombre' id = 'nombre' placeholder = 'Introduce tu nombre' class ='form-control'> " \
                "<input type = 'submit' value = 'Enviar formulario' />" \
                "</form> </body></html>"
            return (httpCode, htmlBody)

        if (comando == "POST"):

            httpCode = "200 OK"
            htmlBody = "<html><body>"\
                        "<h1> Se ha hecho correctamente el POST </h1>"\
                        "</body></html>"


            return (httpCode, htmlBody)



if __name__ == "__main__":
    testWebApp = contentApp("localhost", 1235)
